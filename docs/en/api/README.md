---
title: Introduction
tags: [presentation, start, intro, api]
lang: en
---

# Introduction

LeafCAD is at the cutting edge of technology and uses relatively new programming languages. Currently, we have released a public version of our internal API that works in [GraphQL](https://graphql.org/). We have planned to have a parallel REST API to facilitate the use to all, however, know that the GraphQL API is the one that will be most up-to-date and tested. If you want to use our API to create plugins or other things, we suggest you get acquainted with this new technology.

## GraphQL Learning Ressources

In case you are not familliar with GraphQL, we suggest there learning ressources:

- [https://graphql.org/learn/](https://graphql.org/learn/)
- [https://www.howtographql.com/](https://www.howtographql.com/)
- [https://v1.prisma.io/docs/1.34/](https://v1.prisma.io/docs/1.34/)
- Youtube
- Google
