---
home: true
heroImage: //cdn.leafcad.com/images/logos/leafcad-logos/Logo%20LeafCAD.png
heroText: LeafCAD Docs
tagline: The best CAD/MDT for your community
actionText: API Documentation →
actionLink: /api/
features:
  - title: Customizable
    details: You can customize your radio codes, civilians, vehicles and much more.
  - title: For communities
    details: Equip your community with the best CAD/MDT solution in the world!
  - title: Multilingual
    details: Multiple languages to fit the needs of all your members.
footer: LeafCAD Documentation | Copyright © 2020-present LeafCAD
---
