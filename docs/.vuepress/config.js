module.exports = {
  title: "LeafCAD Documentation",
  description: "Documentation for LeafCAD's public API",
  head: [
    [
      "meta",
      {
        name: "og:image",
        content:
          "https://cdn.leafcad.com/images/logos/leafcad-logos/Logo%20LeafCAD.png",
      },
    ],
  ],
  locales: {
    "/en/": {
      lang: "en",
      title: "LeafCAD Documentation",
      description: "API Documentation for LeafCAD",
    },
    "/fr/": {
      lang: "fr",
      title: "Documentation LeafCAD",
      description: "Documentation de l'API pour LeafCAD",
    },
  },
  themeConfig: {
    logo:
      "https://cdn.leafcad.com/images/logos/leafcad-logos/Logo%20LeafCAD.png",
    editLinks: false,

    locales: {
      "/en/": {
        selectText: "Languages",
        label: "English",
        ariaLabel: "Languages",
        searchPlaceholder: "Search...",

        nav: [
          { text: "Home", link: "/en/" },
          {
            text: "API Reference",
            items: [
              {
                text: "GraphQL API Reference",
                link: "https://graphql-docs.leafcad.com",
              },
            ],
          },
        ],
        sidebarDepth: 3,
        sidebar: {
          "/en/api/": [
            "",
            "graphql",
            "error-codes",
            // {
            //   title: 'GraphQL',
            //   collapsable: false,
            //   children: [['graphql/', 'Introduction'], 'graphql/User'],
            // },
          ],
        },
      },
      "/fr/": {
        selectText: "Langages",
        label: "Français",
        ariaLabel: "Langages",
        searchPlaceholder: "Rechercher...",

        nav: [
          { text: "Accueil", link: "/fr/" },
          {
            text: "Référence de l'API",
            items: [
              {
                text: "Référence de l'API GraphQL",
                link: "https://graphql-docs.leafcad.com",
              },
            ],
          },
        ],
        sidebarDepth: 3,
        sidebar: {
          "/fr/api/": [
            "",
            "graphql",
            "codes-erreur",
            // {
            //   title: 'GraphQL',
            //   collapsable: false,
            //   children: [['graphql/', 'Introduction'], 'graphql/User'],
            // },
          ],
        },
      },
    },
  },
  plugins: [
    [
      "vuepress-plugin-redirect",
      {
        // provide i18n redirection
        // it will automatically redirect `/foo/bar/` to `/:locale/foo/bar/` if exists
        locales: true,
      },
    ],
  ],
};

function getAPISidebar(groupA) {
  return [
    {
      title: groupA,
      collapsable: true,
      children: ["", "gql"],
    },
  ];
}
