# LeafCAD Documentation

LeafCAD's documentation allow all external developers to share the same knowledge on the usage of the APIs and other little quirks of the project.

The documentation is generated using [VuePress](https://vuepress.vuejs.org)

## Installation steps for development

### Necessary software installation

1. Install [Node.js](https://nodejs.org) and npm (should be bundle with Node)
2. Install [Git](https://git-scm.com/)
3. Clone this repo

### Installation of the documentation

1. Execute the command: `npm i`

You should now be able to launch the development server using `npm start`
